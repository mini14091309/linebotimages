const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => res.send('Hello World!'));

app.get("/products", (req,res) => {
    const products = [
        {
            id: 1,
            name: "hammer",
        },
        {
            id: 2,
            name: "screwdriver",
        },
        {
            id: 3,
            name: "wrench",
        },
    ];
    
    res.json(products);

    save('林志堉(Doug)', '生日快樂', 'Doug(林志堉)');
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));



const fs = require('fs').promises;
const { createCanvas, loadImage, registerFont } = require('canvas');

async function draw(one, two, tree) {
    const bg = await loadImage('./assets/images.jpg');
    
    const canvas = createCanvas(bg.width, bg.height);
    const ctx = canvas.getContext('2d');
    
    ctx.drawImage(bg, 0, 0, bg.width, bg.height);
    
    registerFont('./assets/font/1.ttf', { family: 'msyh' });
    
    ctx.fillStyle = '#000';
    ctx.font = 'bold 22px "msyh"';
    ctx.textAlign = 'left';
    ctx.textBaseline = 'top';
    this.textWidt = ctx.measureText(one).width;
    ctx.fillText(one, 10, 10, bg.width);
    
    
    ctx.fillStyle = '#000';
    ctx.font = 'bold 26px "msyh"';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'top';
    ctx.fillText(two, bg.width / 2, bg.height / 3, bg.width);
    
    ctx.fillStyle = '#000';
    ctx.font = 'bold 22px "msyh"';
    ctx.textAlign = 'right';
    ctx.textBaseline = 'top';
    ctx.fillText(tree, bg.width - 10, bg.height / 3 * 2, bg.width);
    
    // const qrcode = await loadImage('./assets/qrcode.png');
    
    // ctx.drawImage(qrcode, (bg.width - qrcode.width) / 2, 400, qrcode.width, qrcode.height);
    
    return canvas;
}

async function save(one, two, tree) {
    try {
        const canvas = await draw(one, two, tree);
        await fs.writeFile('./assets/a.jpg', canvas.toBuffer());
        console.log('保存成功！');
        console.timeEnd('img');
    } catch (e) {
        console.error(e);
    }
}

console.time('img');

